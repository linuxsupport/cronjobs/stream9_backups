#!/bin/bash

TODAY=`/bin/date +%Y%m%d`
SOURCE=/data/$PATH_SNAPSHOTS/s9-snapshots
if [ ! -d $SOURCE/.restic ]; then
  mkdir $SOURCE/.restic
fi
if [ ! -d $SOURCE/.restic/tmpdir ]; then
  mkdir $SOURCE/.restic/tmpdir
fi
if [ ! -d $SOURCE/.restic/cachedir ]; then
  mkdir $SOURCE/.restic/cachedir
fi
export TMPDIR=$SOURCE/.restic/tmpdir
TEMPLATE=email_report.tpl
# s3.connections default is 5, let's increase it for better perf
# and set the cachedir explicitly
RESTIC="restic -o s3.connections=32 --cache-dir $SOURCE/.restic/cachedir"
