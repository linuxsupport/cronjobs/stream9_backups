#!/bin/bash
echo "This script defines variables required to access the S3 repo"
echo "These variables can be found at https://gitlab.cern.ch/linuxsupport/cronjobs/stream9_backups/-/settings/ci_cd"
echo -n "Please enter AWS_ACCESS_KEY_ID: "
read AWS_ACCESS_KEY_ID
echo -n "Please enter AWS_SECRET_ACCESS_KEY: "
read AWS_SECRET_ACCESS_KEY
echo -n "Please enter RESTIC_PASSWORD: "
read RESTIC_PASSWORD

echo "export AWS_ACCESS_KEY_ID=$AWS_ACCESS_KEY_ID" >> /tmp/credentials
echo "export AWS_SECRET_ACCESS_KEY=$AWS_SECRET_ACCESS_KEY" >> /tmp/credentials
echo "export RESTIC_PASSWORD=$RESTIC_PASSWORD" >> /tmp/credentials
echo "Credentials saved to /tmp/credentials"
